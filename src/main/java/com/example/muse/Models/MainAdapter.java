package com.example.muse;

import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final int ITEM_TYPE_OBJET = 1;
    private static final int ITEM_TYPE_STRING = 2;
    public static ArrayList<Equipment> listEquipments;
    private ArrayList<Equipment> saveListEquipments;
    public static Equipment obj;


    public MainAdapter(ArrayList<Equipment> listEquipments){
        this.listEquipments = listEquipments;
        saveListEquipments = new ArrayList<>(listEquipments);
    }

    @Override
    public int getItemViewType(int position) {
        if (listEquipments.get(position) instanceof Equipment) {
            return ITEM_TYPE_OBJET;
        } else {
            return ITEM_TYPE_STRING;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if(viewType == ITEM_TYPE_OBJET){
            View view = inflater.inflate(R.layout.equipment_on_list, parent, false);
            return new MyObjetViewHolder(view);
        }
        else{
            View view = inflater.inflate(R.layout.equipment_on_list, parent, false);
            return new MyTitreViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position){
        obj = listEquipments.get(position);

        if(obj instanceof Equipment){
            ((MyObjetViewHolder) holder).display((Equipment) obj);

        }
        else{
            ((MyTitreViewHolder) holder).display((String) obj);
        }

    }



    @Override
    public int getItemCount(){
        if(listEquipments != null){
            return listEquipments.size();
        }
        else{
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return filtre;
    }
    private Filter filtre = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Equipment> filteredList = new ArrayList<Equipment>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(saveListEquipments);
            }
            else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Equipment item : listEquipments) {
                    if(item instanceof Equipment) {
                        if (((Equipment) item).getName().toLowerCase().contains(filterPattern) /*|| ((Equipment) item).getYear().contains(filterPattern)*/ || TextUtils.join(" - ",((Equipment) item).getCategories()).toLowerCase().contains(filterPattern) /*|| ((Equipment) item).getDescr().toLowerCase().contains(filterPattern) || TextUtils.join(" - ",((Equipment) item).getTimeFrame()).toLowerCase().contains(filterPattern) || ((Equipment) item).getBrand().toLowerCase().contains(filterPattern) || TextUtils.join(" - ",((Equipment) item).getTechDetails()).toLowerCase().contains(filterPattern)*/) {
                            filteredList.add(item);
                        }
                        else if(((Equipment) item).getYear() != null && ((Equipment) item).getYear().contains(filterPattern)){
                            filteredList.add(item);
                        }
                        else if(((Equipment) item).getDescr() != null && ((Equipment) item).getDescr().toLowerCase().contains(filterPattern)){
                            filteredList.add(item);
                        }
                        else if(((Equipment) item).getTimeFrame() != null && TextUtils.join(" - ",((Equipment) item).getTimeFrame()).toLowerCase().contains(filterPattern)){
                            filteredList.add(item);
                        }
                        else if(((Equipment) item).getBrand() != null && ((Equipment) item).getBrand().toLowerCase().contains(filterPattern)){
                            filteredList.add(item);
                        }
                        else if(((Equipment) item).getTechDetails() != null && TextUtils.join(" - ",((Equipment) item).getTechDetails()).toLowerCase().contains(filterPattern)){
                            filteredList.add(item);
                        }
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listEquipments.clear();
            listEquipments.addAll((ArrayList) results.values);
            notifyDataSetChanged();
        }
    };

    public static class MyObjetViewHolder extends RecyclerView.ViewHolder{

        public final TextView name;
        public final TextView categories;
        public final ImageView thumb;

        public MyObjetViewHolder(final View view){
            super(view);
            name = view.findViewById(R.id.name);
            categories = view.findViewById(R.id.categories);
            thumb = view.findViewById(R.id.thumb);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(view.getContext(), ActivityDetail.class);
                    intent.putExtra("OBJET", (Equipment) listEquipments.get(getAdapterPosition()));
                    v.getContext().startActivity(intent);
                }
            });
        }

        public void display(Equipment equipment){
            name.setText(equipment.getName());
            categories.setText(TextUtils.join(" - ", equipment.getCategories()));
            try{
                String urlT = WebServiceUrlMuseum.buildGetThumbnail(equipment.getIdName()).toString();
                Picasso.get().load(urlT).into(thumb);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static class MyTitreViewHolder extends RecyclerView.ViewHolder{

        public final TextView TITLE;

        public MyTitreViewHolder(View view){
            super(view);
            TITLE = view.findViewById(R.id.TITLE);
        }

        public void display(String titre){
            TITLE.setText(titre);
        }
    }
}
